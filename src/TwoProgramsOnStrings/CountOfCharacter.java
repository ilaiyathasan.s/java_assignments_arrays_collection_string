package TwoProgramsOnStrings;

public class CountOfCharacter {

public static void main(String[] args) {
		
		String str = "excellent";
		char search = 'e';
		int count = 0;
		
		for (int i = 0; i < str.length(); i++) {
			if (search==str.charAt(i))
				count++;		
		}
		System.out.println("count of alphabet "+search+ " is " +count);
	}
}
