package TwoProgramsOnCollections;

import java.util.ArrayList;
import java.util.Collections;

public class Descendingorder {

	public static void main(String[] args) {
		
		 ArrayList<String> list = new ArrayList<String>();
		  
	      list.add("Bobby");
	      list.add("Giffy");
	      list.add("Zebra");
	      list.add("Arun");
	      list.add("Yellow");

	      System.out.println("Unsorted ArrayList: " + list);
	      for(String s: list){
	           System.out.println(s);
	        }

	      Collections.sort(list, Collections.reverseOrder());

	      System.out.println("Sorted ArrayList " + "in Ascending order : " + list);
	      for(String str: list){
	           System.out.println(str);
	        }

	}

}
