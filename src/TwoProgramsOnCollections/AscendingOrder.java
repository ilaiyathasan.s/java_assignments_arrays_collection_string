package TwoProgramsOnCollections;

import java.util.ArrayList;
import java.util.Collections;

public class AscendingOrder {

	public static void main(String[] args) {
		
	  ArrayList<String> list = new ArrayList<String>();
		  
      list.add("Bobby");
      list.add("Giffy");
      list.add("Zebra");
      list.add("Arun");
      list.add("Yellow");

      System.out.println("Unsorted ArrayList: " + list);

      Collections.sort(list);

      System.out.println("Sorted ArrayList " + "in Ascending order : " + list);

	}

}
